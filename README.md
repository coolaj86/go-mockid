# go-mockid

OAuth2 / JWT / OpenID Connect for mocking auth... which isn't that different from doing it for real, actually.

## Enabling Google OAuth2 (Mid-2020)

1. Create an account at https://console.developers.google.com/apis/dashboard
2. Go back to https://console.developers.google.com/apis/dashboard
3. Create a New Project from the dropdown in the upper left that lists the current project name
  4. Give the project a name such as `Example Web App` and accept its generated ID
  5. Click "Create"

Add your test domain

1. Go back to https://console.developers.google.com/apis/dashboard
1. Select your new project from the upper-left drop-down
2. Select `Domain Verification` from the left hand side of the screen
3. Add your test domain (i.e. `beta.example.com`), but a domain that you actually own
4. Select `Verify Ownership`
5. Follow the specific instructions for adding a txt record to the subdomain you chose
6. Add a collaborator / co-owner if you wish

Enable OAuth2

1. Go back to https://console.developers.google.com/apis/dashboard
1. Select `OAuth consent screen`
2. Select `External`
3. Complete the consent screen form

Create Google Credentials

1. Go back to https://console.developers.google.com/apis/dashboard
1. Select `Credentials` from the left sidebar
2. Select `OAuth ID`
3. Select `Web Application`
4. Fill out the same test domain and test app name as before
5. Save the ID and Secret to a place you won't forget (perhaps a .gitignored .env)

Update your signin page.

1. You need to put your default scopes (i.e. `profile email`) and client ID in the meta tag of your login page HTML. `profile` is the minimum scope and is always returned.
  ```html
  <head>
    <meta name="google-signin-scope" content="email">
    <meta
      name="google-signin-client_id"
      content="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com"
    />
  </head>
  ```
2. Although it should be possible to use an thin OAuth client, you'll probably want to start by including the (huge) Google platform.js
  ```html
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  ```
3. You can start off with the Google's sign in button, but you need your own `data-onsuccess` callback. You can also adjust the `data-scope` per button to include more stuff. Scopes are defined at https://developers.google.com/identity/protocols/oauth2/scopes
  ```html
  <div
    class="g-signin2"
    data-onsuccess="ongsignin"
    data-scope="profile email https://www.googleapis.com/auth/spreadsheets.readonly https://www.googleapis.com/auth/drive.readonly"
  ></div>
  <script>
    window.ongsignin = function (gauth) {
      // Note: this is a special prototype-style instance object with few
      // enumerable properties (which don't make sense). Requires API docs.
      // See https://developers.google.com/identity/sign-in/web
      console.log(goauth)
    };
  </script>
  ```
4. Despite the documentation stating that passing a token as a query is deprecated and to use the `Authorization` header, the inspect token URL only supports the query parameter: `GET https://oauth2.googleapis.com/tokeninfo?id_token=<token>`
  - You can also validate the token with Google's public key
  - https://accounts.google.com/.well-known/openid-configuration
  - https://www.googleapis.com/oauth2/v3/certs (note that one of the Key IDs will match that of your kid)
5. While testing you'll probably want to revoke the app's permissions
  - Go to https://myaccount.google.com/permissions
  - Under "Third-party apps with account access" click "Manage third-party access" and search in the long list and click "Remove access".
  - Under "Signing in to other sites" click "Signing in with Google" and search in the list to revoke access
  - Active tokens will persist until they expire (1 hour), so you may need to clear cache, cookies, etc, which can be a pain
5. Sign out can be accomplished with a button that calls `gapi.auth2.getAuthInstance().signOut().then(function() { });`

