package xkeypairs

import (
	"io/ioutil"

	"git.rootprojects.org/root/keypairs"
)

// ParsePEMPrivateKey will parse a PEM Private Key (or JWK or DER) but in future versions will fail to parse other key input types
func ParsePEMPrivateKey(block []byte) (keypairs.PrivateKey, error) {
	// TODO do not parse DER or JWK
	return keypairs.ParsePrivateKey(block)
}

// ParsePrivateKeyFile returns the private key from the given file path, if available
func ParsePrivateKeyFile(pathname string) (keypairs.PrivateKey, error) {
	block, err := ioutil.ReadFile(pathname)
	if nil != err {
		return nil, err
	}
	return keypairs.ParsePrivateKey(block)
}
