package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"git.coolaj86.com/coolaj86/go-mockid/mockid"
	"git.rootprojects.org/root/keypairs"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	done := make(chan bool)
	var port int
	var host string

	rand.Seed(time.Now().UnixNano())

	portFlag := flag.Int("port", 0, "Port on which the HTTP server should run")
	urlFlag := flag.String("url", "", "Outward-facing address, such as https://example.com")
	prefixFlag := flag.String("jwkspath", "", "The path to the JWKs storage directory")
	flag.Parse()

	if nil != portFlag && *portFlag > 0 {
		port = *portFlag
	} else {
		portStr := os.Getenv("PORT")
		port, _ = strconv.Atoi(portStr)
	}
	if port < 1 {
		fmt.Fprintf(os.Stderr, "You must specify --port or PORT\n")
		os.Exit(1)
	}

	jwkpath := "./default.jwk.json"
	jwkb, err := ioutil.ReadFile(jwkpath)
	if nil != err {
		panic(fmt.Errorf("read default jwk %v: %w", jwkpath, err))
		return
	}

	privkey, err := keypairs.ParseJWKPrivateKey(jwkb)
	if nil != err {
		// TODO delete the bad file?
		panic(fmt.Errorf("unmarshal jwk %v: %w", string(jwkb), err))
		return
	}

	if nil != urlFlag && "" != *urlFlag {
		host = *urlFlag
	} else {
		host = "http://localhost:" + strconv.Itoa(port)
	}

	var jwksPrefix string
	if nil != prefixFlag && "" != *prefixFlag {
		jwksPrefix = *prefixFlag
	} else {
		jwksPrefix = "public-jwks"
	}
	err = os.MkdirAll(jwksPrefix, 0755)
	if nil != err {
		fmt.Fprintf(os.Stderr, "couldn't write %q: %s", jwksPrefix, err)
		os.Exit(1)
	}

	mux := mockid.Route(jwksPrefix, privkey)

	fs := http.FileServer(http.Dir("./public"))
	http.Handle("/", fs)
	/*
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			log.Printf(r.Method, r.URL.Path)
			http.Error(w, "Not Found", http.StatusNotFound)
		})
	*/

	fmt.Printf("Serving on port %d\n", port)
	go func() {
		log.Fatal(http.ListenAndServe(":"+strconv.Itoa(port), mux))
		done <- true
	}()

	// TODO privB := keypairs.MarshalJWKPrivateKey(privkey)
	privB := keypairs.MarshalJWKPrivateKey(privkey)
	fmt.Printf("Private Key:\n\t%s\n", string(privB))
	pubB := keypairs.MarshalJWKPublicKey(keypairs.NewPublicKey(privkey.Public()))
	fmt.Printf("Public Key:\n\t%s\n", string(pubB))
	protected, payload, token := mockid.GenToken(host, privkey, url.Values{})
	fmt.Printf("Protected (Header):\n\t%s\n", protected)
	fmt.Printf("Payload (Claims):\n\t%s\n", payload)
	fmt.Printf("Access Token:\n\t%s\n", token)

	<-done
}
