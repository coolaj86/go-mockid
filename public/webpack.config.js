'use strict';

var path = require('path');

module.exports = {
	entry: './main.js',
	mode: 'development',
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		port: 3001
	},
	output: {
		publicPath: 'http://localhost:3001/'
	},
	module: {
		rules: [{}]
	},
	plugins: []
};
