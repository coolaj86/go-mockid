module git.coolaj86.com/coolaj86/go-mockid

go 1.13

require (
	git.rootprojects.org/root/hashcash v1.0.1
	git.rootprojects.org/root/keypairs v0.6.5
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/mailgun/mailgun-go/v3 v3.6.4
	github.com/mileusna/useragent v1.0.2
)
