package api

import (
	"net/http"

	"git.coolaj86.com/coolaj86/go-mockid/xkeypairs"
	"git.rootprojects.org/root/keypairs"
)

// GeneratePublicJWK will create a new private key in JWK format
func GeneratePublicJWK(w http.ResponseWriter, r *http.Request) {
	if "POST" != r.Method {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	opts, err := getOpts(r)
	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	privkey, err := getPrivKey(opts)
	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	jwk := keypairs.MarshalJWKPublicKey(keypairs.NewPublicKey(privkey.Public()))
	w.Write(append(jwk, '\n'))
}

// GeneratePrivateJWK will create a new private key in JWK format
func GeneratePrivateJWK(w http.ResponseWriter, r *http.Request) {
	if "POST" != r.Method {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	opts, err := getOpts(r)
	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	privkey := xkeypairs.GenPrivKey(opts)

	jwk := keypairs.MarshalJWKPrivateKey(privkey)
	w.Write(append(jwk, '\n'))
}

// GeneratePublicDER will create a new private key in JWK format
func GeneratePublicDER(w http.ResponseWriter, r *http.Request) {
	if "POST" != r.Method {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	opts, err := getOpts(r)
	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	privkey, err := getPrivKey(opts)
	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	b, _ := keypairs.MarshalDERPublicKey(privkey.Public())

	w.Write(b)
}

// GeneratePrivateDER will create a new private key in a valid DER encoding
func GeneratePrivateDER(w http.ResponseWriter, r *http.Request) {
	if "POST" != r.Method {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	opts, err := getOpts(r)
	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	privkey := xkeypairs.GenPrivKey(opts)

	der, _ := keypairs.MarshalDERPrivateKey(privkey)
	w.Write(der)
}

// GeneratePublicPEM will create a new private key in JWK format
func GeneratePublicPEM(w http.ResponseWriter, r *http.Request) {
	if "POST" != r.Method {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	opts, err := getOpts(r)
	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	privkey, err := getPrivKey(opts)
	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	b, _ := keypairs.MarshalPEMPublicKey(privkey.Public())

	w.Write(b)
}

// GeneratePrivatePEM will create a new private key in a valid PEM encoding
func GeneratePrivatePEM(w http.ResponseWriter, r *http.Request) {
	if "POST" != r.Method {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	opts, err := getOpts(r)
	if nil != err {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	privkey := xkeypairs.GenPrivKey(opts)

	privpem, _ := keypairs.MarshalPEMPrivateKey(privkey)
	w.Write(privpem)
}

const maxRetry = 16

func getPrivKey(opts *xkeypairs.KeyOptions) (keypairs.PrivateKey, error) {
	if "" != opts.Key {
		return keypairs.ParsePrivateKey([]byte(opts.Key))
	}
	return xkeypairs.GenPrivKey(opts), nil
}
