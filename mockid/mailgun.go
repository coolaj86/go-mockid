package mockid

import (
	"context"
	"os"
	"time"

	mailgun "github.com/mailgun/mailgun-go/v3"

	_ "github.com/joho/godotenv/autoload"
)

var (
	mgDomain string
	mgAPIKey string
	mgFrom   string
	mg       *mailgun.MailgunImpl
)

func init() {
	/*
	  MAILGUN_API_KEY=key-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	  MAILGUN_DOMAIN=mail.example.com
	  MAILER_FROM="Rob the Robot <rob.the.robot@mail.example.com>"
	*/

	mgDomain = os.Getenv("MAILGUN_DOMAIN")
	mgAPIKey = os.Getenv("MAILGUN_API_KEY")
	mgFrom = os.Getenv("MAILER_FROM")

	mg = mailgun.NewMailgun(mgDomain, mgAPIKey)
}

func SendSimpleMessage(to, from, subject, text, replyTo string) (string, error) {
	m := mg.NewMessage(from, subject, text, to)
	if 0 != len(replyTo) {
		// mailgun's required "h:" prefix is added by the library
		m.AddHeader("Reply-To", replyTo)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	_, id, err := mg.Send(ctx, m)
	return id, err
}
