package kvdb

import (
	"strings"
	"testing"
)

type TestEntry struct {
	Email    string   `json:"email"`
	Subjects []string `json:"subjects"`
}

var email = "john@example.com"
var sub = "id123"
var dbPrefix = "../testdb"
var testKV = &KVDB{
	Prefix: dbPrefix + "/test-entries",
	Ext:    "eml.json",
}

func TestStore(t *testing.T) {
	entry := &TestEntry{
		Email:    email,
		Subjects: []string{sub},
	}

	if err := testKV.Store(email, entry); nil != err {
		t.Fatal(err)
		return
	}

	value, ok, err := testKV.Load(email, &(TestEntry{}))
	if nil != err {
		t.Fatal(err)
		return
	}
	if !ok {
		t.Fatal("test entry not found")
	}

	v, ok := value.(*TestEntry)
	if !ok {
		t.Fatal("test entry not of type TestEntry")
	}

	if email != v.Email || sub != strings.Join(v.Subjects, ",") {
		t.Fatalf("value: %#v", v)
	}
}

func TestNoExist(t *testing.T) {
	value, ok, err := testKV.Load("not"+email, &(TestEntry{}))
	if nil != err {
		t.Fatal(err)
		return
	}
	if ok {
		t.Fatal("found entry that doesn't exist")
	}
	if value != nil {
		t.Fatal("had value for entry that doesn't exist")
	}
}
