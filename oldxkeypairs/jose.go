package xkeypairs

import (
	"encoding/base64"
	"encoding/json"
	"errors"
)

func (jws *JWS) DecodeComponents() error {
	protected, err := base64.RawURLEncoding.DecodeString(jws.Protected)
	if nil != err {
		return errors.New("invalid JWS header base64Url encoding")
	}
	if err := json.Unmarshal([]byte(protected), &jws.Header); nil != err {
		return errors.New("invalid JWS header")
	}

	payload, err := base64.RawURLEncoding.DecodeString(jws.Payload)
	if nil != err {
		return errors.New("invalid JWS payload base64Url encoding")
	}
	if err := json.Unmarshal([]byte(payload), &jws.Claims); nil != err {
		return errors.New("invalid JWS claims")
	}

	return nil
}

/*
func Decode(msg string) (*JWS, error) {
	jws := &JWS{}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(jws)
	return jws, err
}

func Unmarshal(msg string) (*JWS, error) {
	jws := &JWS{}

	if err := json.Unmarshal([]byte(msg), jws); nil != err {
		return nil, err
	}
	return jws, nil
}
*/
